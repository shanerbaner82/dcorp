<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
    $user = User::first();
    $admin = Role::whereName('admin')->first();
    return $user->roles;
});

Route::get('reporting', function(){
    return 'secret!';
})->before('role:member');
